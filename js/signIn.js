// Variables
const logInAccountBtn = document.querySelector("#logIntoAccountBtn");
const createVisit = document.querySelector("#createVisit");

const formSignIn = document.querySelector("#formSignIn");

// TOKEN
let token;


// EventListener
logInAccountBtn.addEventListener("click", createSignInForm);


// Functions
function createSignInForm() {
    const createForm = new Form({
        element: formSignIn,
        innerHtmlMessage: `   
                    <div class="container">
                        <div class="form__wrapper">
                            <div class="form__input-wrapper">
                                <input type="email" placeholder="Email" id="inputSignInEmail" value="your.mail@gmail.com">
                                <input type="text" placeholder="Password" id="inputSignInPassword" value="12344344аа">
                            </div>
                            
                            <button id="btnSignIn">Sign in</button>

                            <div class="form__alert">Введен неправельный пароль или emal!!!</div>
                        </div>
                    </div>
                `,

    });
    createForm.showForm();

    const inputSignInEmail = document.querySelector("#inputSignInEmail");
    const inputSignInPassword = document.querySelector("#inputSignInPassword");
    const formAlert = document.querySelector(".form__alert");
    const btnSignIn = document.querySelector("#btnSignIn");

    btnSignIn.addEventListener("click", () => {
        signInServer(formSignIn, inputSignInEmail.value, inputSignInPassword.value, formAlert);
    })
}

async function signInServer(form, email, password, error) {
    try {
        const user = {
            "email": email,
            "password": password,
        }
        const resonse = await fetch('https://ajax.test-danit.com/api/cards/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(user),
        })

        token = await resonse.text()

        if (resonse.status > 400) {
            error.classList.add("active");
        } else {
            form.style.display = "none";
            logInAccountBtn.classList.remove("active");
            createVisit.classList.add("active");
        }

    } catch (err) {
        error.classList.add("active");
        error.textContent = `Что-то пошло не так`;
        console.log(err)
    }
}