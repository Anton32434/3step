// Create form class
class Form {
    constructor(options) {
        [this.innerHtmlMessage, this.element] = [options.innerHtmlMessage, options.element];
    }

    showForm() {
        const element = this.element;
        element.classList.add("active");
        element.innerHTML = this.innerHtmlMessage;
    }

    closeForm() {
        const element = this.element;
        element.classList.remove("active");
        element.style.display = "none";
    }
}

class Card {
    constructor(option) {
        this.doctor = option.doctor;
        this.name = option.name;
        this.form = option.form;
    }

    createCard() {
        const cardsList = document.querySelector(".cards__list");
        cardsList.innerHTML += `
                    <li class="cards__item">
                        <h2 class="cards__item-doctor">
                            <span>Врач:</span> 
                            <span class="cardDoctor">${this.doctor}</span>
                        </h2>
                        <p class="cards__item-visitor-name">
                            <span>Ваше имя:</span>
                            <span class="cardVisitorName">${this.name}</span>
                        </p>

                        <button class="cardOpenShowMoreBtn active">Показать больше</button>
                        <button class="cardsCloseShowMoreBtn">Закрыть</button>
                        <button class="cardEditBtn">Редактировать</button>
                        <button class="cardDeliteBtn">Удалить</button>
                    </li>
        `

        // Delete form
        const form = this.form;
        form.classList.remove("active")
    }
}





class Input {
    constructor(obj) {
        this.element = obj.element; 
        this.type = obj.type;
        this.value = obj.value;
        this.placeholder = obj.placeholder;
        this.classList = obj.classList;
    }

    createInput() {
        const input = document.createElement("input");
        input.value = this.value;
        input.placeholder = this.placeholder;
        input.classList.add(this.classList);
        input.type = this.type;

        const element = this.element;

        element.appendChild(input)
    }
}

class TextArea {
    constructor(obj) {
        this.element = obj.element;
        this.classList = obj.classList;
    }

    createTextArea() {
        const textArea = document.createElement("textarea");
        const element = this.element;

        element.appendChild(textArea)
    }
}

class Select {
    constructor(obj, arrOptions) {
        this.element = obj.element;
        this.classList = obj.classList;
    }

    createSelect() {
        const select = document.createElement("select");
        
        for(let key of arrOptions) {
            const option = document.createElement("option");
            option.value = arrOptions[key];
            option.textContent = arrOptions[key];
            option.classList.add(this.classList);

            select.appendChild(option);
        }

        const element = this.element;

        element.appendChild(select);
    }
}
