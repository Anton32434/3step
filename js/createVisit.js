// Variables
const formCreateVisit = document.querySelector("#formCreateVisit");
const createVisitBtn = document.querySelector("#createVisit");

const formDoctorAppointmentBtn = document.querySelector("#formDoctorAppointmentBtn");


// EventListener
createVisitBtn.addEventListener("click", createVisitForm);


// Functions
function createVisitForm() {
    // Variables
    const createVisit = new Form({
        element: formCreateVisit,
        innerHtmlMessage: `
        <div class="form__wrapper form__create-visit-wrapper">
            <div class="form__select-wrapper">
                <label class="form__select-title">Выберите врача</label>

                <select required id="chooseDoctor">
                    <option value="Cardiologist">Кардиолог</option>
                    <option value="Dentist">Стоматолог</option>
                    <option value="Therapist">Терапевт</option>
                </select>
            </div>

            <div class="form__input-wrapper">
                <input type="text" placeholder="Какая цель везита" id="formTitle" value="some" required>
            </div>

            <div class="form__textareaid-wrapper">
                <label class="form__title">Краткое описание визита</label>

                <textarea cols="30" rows="10" id="formDescription" value="some" required></textarea>
            </div>

            <div class="form__select-wrapper">
                <label class="form__select-title">Выберите срочность</label>

                <select required>
                    <option>Обычная</option>
                    <option>Приоритетная</option>
                    <option>Неотложная</option>
                </select>
            </div>

            <div class="form__input-wrapper">
                <input type="text" data-doctor="Cardiologist" class="form__input-create-visit active" id="fullName" placeholder="Укажите ваше ФИО" value="some">
                <input type="text" data-doctor="Cardiologist" class="form__input-create-visit active" id="Pressure" placeholder="Какое у вас обычное давление?" value="some">
                <input type="text" data-doctor="Cardiologist" class="form__input-create-visit active" id="BodyMassIndex" placeholder="Какой у вас индекс массы тела" value="some">
                <input type="text" data-doctor="Cardiologist" class="form__input-create-visit active" id="cardiovascularDiseases" placeholder="Были у вас сердечно-сосудистые заболевания?" value="some">
                <input type="text" data-doctor="Cardiologist" class="form__input-create-visit active" id="age" placeholder="Укажите ваш возраст" value="some">

                <input type="text" data-doctor="Dentist" class="form__input-create-visit" id="fullName" placeholder="Укажите ваше ФИО" value="some">
                <input type="text" data-doctor="Dentist" class="form__input-create-visit" id="lastVisit" placeholder="Укажите дату последнего посещения" value="some">

                <input type="text" data-doctor="Therapist" class="form__input-create-visit" id="fullName" placeholder="Укажите ваше ФИО" value="some">
                <input type="text" data-doctor="Therapist" class="form__input-create-visit" id="age" placeholder="Укажите ваш возраст" value="some">
            </div>

            <button id="formDoctorAppointmentBtn">Записаться к врачу</button>
            <button id="closeFormCreateVisit">Закрыть</button>
        </div>
        `
    })
    createVisit.showForm();

    const chooseDoctor = document.querySelector("#chooseDoctor");
    let doctores = chooseDoctor.value;
    const fullName = document.querySelector("#fullName");

    const formDoctorAppointmentbtn = document.querySelector("#formDoctorAppointmentBtn");

    const createVisitInputs = document.querySelectorAll(".form__input-create-visit");

    // Send a request to the server
    const formTitle = document.querySelector("#formTitle");
    const formDescription = document.querySelector("#formDescription");

    const closeFormCreateVisit = document.querySelector("#closeFormCreateVisit");



    // EventListener
    chooseDoctor.addEventListener("change", changeOptions);

    formDoctorAppointmentbtn.addEventListener("click", () => {
        sendVisitToTheServer(formTitle.value, formDescription.value, doctores, token);
    })

    closeFormCreateVisit.addEventListener("click", () => createVisit.closeForm());
}

function changeOptions() {
    const createVisitInputs = document.querySelectorAll(".form__input-create-visit");
    doctores = chooseDoctor.value;

    createVisitInputs.forEach(input => {
        const createVisitInputsAttribute = input.getAttribute("data-doctor");

        if (doctores === createVisitInputsAttribute) {
            input.classList.add("active");
        } else {
            input.value = '';
            input.classList.remove("active");
        }
    })
}

async function sendVisitToTheServer(title, description, doctor, token) {
    try {
        const visit = {
            title: title,
            description: description,
            doctor: doctor,
        }

        const createVisitInputs = document.querySelectorAll(".form__input-create-visit");

        // Add input value in visit
        createVisitInputs.forEach(input => {
            const inputValue = input.value;
            const checkForEmptyInput = inputValue.trim();

            if (checkForEmptyInput.length !== 0) {
                const inputId = input.id;
                visit[inputId] = inputValue;
            }
        })
        // /Add input value in visit
    } catch (err) {
        console.error(err);
    }
}