// Variables
const cards__item = document.querySelectorAll(".cards__item");

const cardDoctor = document.querySelectorAll(".cardDoctor");
const cardVisitorName = document.querySelectorAll(".cardVisitorName");

// buttons 
const cardOpenShowMoreBtn = document.querySelectorAll(".cardOpenShowMoreBtn");
const cardsCloseShowMoreBtn = document.querySelectorAll(".cardsCloseShowMoreBtn");
const cardEditBtn = document.querySelectorAll(".cardEditBtn");
const cardDeliteBtn = document.querySelectorAll(".cardDeliteBtn");



// Open "show more"
cardOpenShowMoreBtn.forEach((btn, index) => {
    btn.addEventListener("click", () => {
        btn.classList.remove("active");

        cardsCloseShowMoreBtn[index].classList.add("active");
        cardEditBtn[index].classList.add("active");
        cardDeliteBtn[index].classList.add("active");
    })
})



// Close "show more"
cardsCloseShowMoreBtn.forEach((btn, index) => {
    btn.addEventListener("click", () => {
        btn.classList.remove("active");
        cardEditBtn[index].classList.remove("active");
        cardDeliteBtn[index].classList.remove("active");

        cardOpenShowMoreBtn[index].classList.add("active");
    })
})

// Delite card 
cardDeliteBtn.forEach((btn, index) => {
    btn.addEventListener("click", () => {
        cards__item[index].style.display = "none";
    })
})
